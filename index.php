<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=B612+Mono&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Fira+Mono&display=swap" rel="stylesheet">
		<style>
			* {
				font-family: 'Fira Mono', monospace;
				border-collapse: collapse;
			}
			table {
				border: 3px solid black;
			}
			th, td {
				border: 2px solid black;
			}
			td {
				text-align: center;
				font-size: 20px;
			}
		</style>
	</head>

<?php
$filelist = glob('*'); #NOTE: Change this to change the files this displays
$directories = array();
$files = array();
foreach($filelist as $filename) {
	if (is_dir($filename)) {
		array_push($directories, $filename);
	} else {
		array_push($files, $filename);
	}
}


echo "<table style='width:100%'>";
echo "<tr>";
echo "<th>Filetype</th>";
echo "<th>File Name</th>";
echo "<th>File Size</th>";
echo "</tr>";

#<tr> <td>file type</td> <td>file name</td> </tr>

foreach($directories as $filepath) {
	$filepath_exploded = explode("/", $filepath);
	$filename = $filepath_exploded[sizeof($filepath_exploded) - 1];
	echo "<tr><td>DIR</td><td><a " . "href=" . $filepath  . " action=files/" . $filepath . ">$filename</a></td><td>" . filesize($filepath) . "B</td></tr>";
}
foreach($files as $filepath) {
	$filepath_exploded = explode("/", $filepath);
	$filename = $filepath_exploded[sizeof($filepath_exploded) - 1];
	echo "<script>console.log('" . "$filename" . "');</script>";
	echo "<tr><td>FILE</td><td><a " . "href=" . $filepath  . " action=files/" . $filepath . ">$filename</a></td><td>" . filesize($filepath) . "B</td></tr>";
}
?>
</html>
